package com.bubnii.model.plateau;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RealPlateauTest {


    @Test
    public void testAppendNextValue() {
        RealPlateau realPlateau = new RealPlateau(2, 4);
        assertEquals(realPlateau, realPlateau.appendNextValue(4));
    }

    @Test
    public void testCompareTo() {
        RealPlateau realPlateau = new RealPlateau(4, 4);
        assertEquals(realPlateau.compareTo(realPlateau), 0);
    }

    @Test
    public void testCompareCountTo() {
        RealPlateau realPlateau = new RealPlateau(0, 0);
        assertEquals(realPlateau.compareCountTo(5), 5);
    }
}