package com.bubnii.model.plateau;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class NonPlateauTest {

    private NonPlateau nonPlateau = new NonPlateau(3, 4);

    @Test
    public void testCompareTo() {
        assertEquals(nonPlateau.compareTo(nonPlateau), 0);
    }

    @Test
    public void testAppendNextValue() {
        assertEquals(nonPlateau.appendNextValue(3), nonPlateau);
    }

    @Test
    public void testCompareCountTo() {
        assertEquals(nonPlateau.compareCountTo(3), 4);
    }
}