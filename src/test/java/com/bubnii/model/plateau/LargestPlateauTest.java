package com.bubnii.model.plateau;

import org.junit.Test;

import static org.junit.Assert.*;

public class LargestPlateauTest {

    @Test
    public void testFindLongestPlateauIn() {
        LargestPlateau largestPlateau = new LargestPlateau();
        int[] arrayValue = new int[]{1, 2, 3, 3, 3, 4, 5, 5, 4};
        RealPlateau plateauOne = (RealPlateau) largestPlateau.findLongestPlateauIn(arrayValue);

        assertEquals(plateauOne.getValue(), 5);
        assertEquals(plateauOne.getStartIndex(), 6);
        assertEquals(plateauOne.getConsecutiveValueCount(), 2);
    }
}