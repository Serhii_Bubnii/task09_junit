package com.bubnii.model.minesweeper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class MinesweeperImplTest {

    @Mock
    Minesweeper minesweeper;

    @InjectMocks
    MinesweeperMockito mockito = new MinesweeperMockito(minesweeper);

    @Test
    public void testBombsAdjacentToCell() {
        boolean[][] bombs = new boolean[][]{{false, false, false, false, false},
                {false, true, false, false, false}, {false, false, false, true, false},
                {false, true, false, false, false}, {false, false, false, false, false}};
        int[][] sol = new int[][]{{0, 0, 0, 0, 0}, {0, 1, 2, 1, 0}, {0, 2, 3, 1, 0}, {0, 1, 2, 1, 0}, {0, 0, 0, 0, 0}};
        when(mockito.bombsAdjacentToCell(3, 3, bombs)).thenReturn(sol);
        assertEquals(minesweeper.bombsAdjacentToCell(3, 3, bombs), sol);
        doReturn(sol).when(minesweeper).bombsAdjacentToCell(3, 3, bombs);
        verify(minesweeper).bombsAdjacentToCell(3, 3, bombs);
    }


    @Test
    public void testHandleBoundaryCases() {
        boolean[][] bombs = new boolean[][]{{false, false, false, false, false}, {false, false, false, false, false},
                {false, false, true, true, false}, {false, false, false, true, false}, {false, false, false, false, false}};
        when(minesweeper.handleBoundaryCases(3, 3, 0.3)).thenReturn(bombs);
        assertEquals(minesweeper.handleBoundaryCases(3, 3, 0.3), bombs);
        doReturn(bombs).when(minesweeper).handleBoundaryCases(3, 3, 0.3);
        verify(minesweeper).handleBoundaryCases(3, 3, 0.3);
    }
}