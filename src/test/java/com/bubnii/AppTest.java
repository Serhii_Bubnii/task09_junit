package com.bubnii;

import com.bubnii.model.plateau.LargestPlateauTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(value= Suite.class)
@Suite.SuiteClasses(value={LargestPlateauTest.class})
public class AppTest {

}