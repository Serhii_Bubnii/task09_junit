package com.bubnii.controller;

import com.bubnii.model.minesweeper.MinesweeperImpl;
import com.bubnii.model.plateau.LargestPlateau;
import com.bubnii.model.plateau.Plateau;

public class Controller {

    private LargestPlateau largestPlateau;
    private MinesweeperImpl minesweeper;

    public Controller() {
        largestPlateau = new LargestPlateau();
        minesweeper = new MinesweeperImpl();
    }

    public Plateau findLongestPlateau(int[] valueList) {
        return largestPlateau.findLongestPlateauIn(valueList);
    }

    public void createMinesweeper(int row, int column, double probability) {
        minesweeper.startGame(row, column, probability);
    }
}
