package com.bubnii.model.plateau;

import java.util.HashSet;
import java.util.Set;

public class LargestPlateau {

    private static final int SKIP_FIRST_VALUE = 1;

    public Plateau findLongestPlateauIn(int[] valueList) {
        Set<Plateau> plateaus = findAllPlateaus(valueList);
        return selectLongestPlateau(plateaus);
    }

    private Plateau selectLongestPlateau(Set<Plateau> plataeus) {
        return plataeus.stream().max(Comparable::compareTo).get();
    }

    private Set<Plateau> findAllPlateaus(int[] valueList) {
        Set<Plateau> plateaus = new HashSet<>();
        Plateau currentPlateau = new RealPlateau(0, valueList[0]);
        plateaus.add(currentPlateau);
        for (int i = SKIP_FIRST_VALUE; i < valueList.length; i++) {
            Plateau nextPlateau = currentPlateau.appendNextValue(valueList[i]);
            if (currentPlateau != nextPlateau) {
                plateaus.add(nextPlateau);
                currentPlateau = nextPlateau;
            }
        }
        return plateaus;
    }
}