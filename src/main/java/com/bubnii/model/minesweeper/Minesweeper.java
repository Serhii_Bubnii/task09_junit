package com.bubnii.model.minesweeper;

public interface Minesweeper {

    int[][] bombsAdjacentToCell(int m, int n, boolean[][] bombs);

    boolean[][] handleBoundaryCases(int m, int n, double p);

}
