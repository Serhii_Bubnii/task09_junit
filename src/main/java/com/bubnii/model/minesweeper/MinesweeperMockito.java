package com.bubnii.model.minesweeper;

public class MinesweeperMockito {

    Minesweeper minesweeper;

    public MinesweeperMockito(Minesweeper minesweeper) {
        this.minesweeper = minesweeper;
    }

    void startGame(int row, int column, double probability) {
        minesweeper.startGame(row, column, probability);
    }

    int[][] bombsAdjacentToCell(int m, int n, boolean[][] bombs) {
        return minesweeper.bombsAdjacentToCell(m, n, bombs);
    }

    boolean[][] handleBoundaryCases(int m, int n, double p) {
        return minesweeper.handleBoundaryCases(m, n, p);
    }

    void indexesNeighboringCells(boolean[][] bombs, int[][] sol, int i, int j) {
        minesweeper.indexesNeighboringCells(bombs, sol, i, j);
    }
}
