package com.bubnii.model.minesweeper;

import java.util.Arrays;

public class MinesweeperImpl implements Minesweeper {

    public void startGame(int row, int column, double probability) {
        int M = row;
        int N = column;
        double p = probability;
        // game grid is [1..M][1..N], border is used to handle boundary cases
        boolean[][] bombs = handleBoundaryCases(M, N, p);
        printGame(M, N, bombs);
        // sol[i][j] = # bombs adjacent to cell (i, j)
        int[][] sol = bombsAdjacentToCell(M, N, bombs);
        System.out.println();
        printSolution(M, N, bombs, sol);
    }

    @Override
    public int[][] bombsAdjacentToCell(int m, int n, boolean[][] bombs) {
        int[][] sol = new int[m + 2][n + 2];
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                // (ii, jj) indexes neighboring cells
                indexesNeighboringCells(bombs, sol, i, j);
            }
        }
        return sol;
    }

    @Override
    public boolean[][] handleBoundaryCases(int m, int n, double p) {
        boolean[][] bombs = new boolean[m + 2][n + 2];
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                bombs[i][j] = (Math.random() < p);
            }
        }
        System.out.println(Arrays.deepToString(bombs));
        return bombs;
    }

    private void indexesNeighboringCells(boolean[][] bombs, int[][] sol, int i, int j) {
        for (int ii = i - 1; ii <= i + 1; ii++) {
            for (int jj = j - 1; jj <= j + 1; jj++) {
                if (bombs[ii][jj]) {
                    sol[i][j]++;
                }
            }
        }
    }

    private void printSolution(int m, int n, boolean[][] bombs, int[][] sol) {
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                if (bombs[i][j]) {
                    System.out.print("* ");
                } else {
                    System.out.print(sol[i][j] + " ");
                }
            }
            System.out.println();
        }
    }

    private void printGame(int m, int n, boolean[][] bombs) {
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                if (bombs[i][j]) {
                    System.out.print("* ");
                } else {
                    System.out.print(". ");
                }
            }
            System.out.println();
        }
    }
}
