package com.bubnii.view;

import com.bubnii.controller.Controller;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {

    private Controller controller = new Controller();
    private ViewPrint printView = new ViewPrint();
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private Scanner input;
    private int[] array = new int[]{1, 2, 3, 4, 5, 6};
    private int[] array1 = new int[]{1, 2, 3, 3, 3, 4, 5, 5, 4};
    private int[] array2 = new int[]{1, 2, 3, 3, 3, 2, 5, 5, 4};
    private static final String LONGEST_PLATEAU_IS = "longest Plateau is: %s";


    public View() {
        input = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        menu.put("1", "\t1 - Show the longest plateau of equals numbers");
        menu.put("2", "\t2 - Launch the game minesweeper");
        menu.put("Q", "\tQ - exit");
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::showLongestPlateau);
        methodsMenu.put("2", this::showMinesweeper);
    }

    private void showLongestPlateau() {
        printView.print(String.format(LONGEST_PLATEAU_IS, controller.findLongestPlateau(array)));
    }

    private void showMinesweeper() {
        printView.print("Enter the values of the row");
        int row = input.nextInt();
        printView.print("Enter the values of the column");
        int column = input.nextInt();
        printView.print("Enter the values of the probability");
        double probability = Double.parseDouble(input.next());
        controller.createMinesweeper(row, column, probability);
    }

    public void show() {
        String keyMenu;
        while (true) {
            printView.print("------------------------------------------------------------------------------------------");
            outputMenu();
            printView.print("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            completeProgram(keyMenu);
            try {
                methodsMenu.get(keyMenu).print();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private void completeProgram(String keyMenu) {
        if (keyMenu.equals("Q")) {
            System.exit(0);
        }
    }

    private void outputMenu() {
        printView.print("MENU:");
        for (String str : menu.values()) {
            printView.print(str);
        }
    }
}

